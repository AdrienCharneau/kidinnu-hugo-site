---
weight: 1
images:
- https://i.imgur.com/NgwuBXt.jpg
- https://i1.sndcdn.com/artworks-000120316565-g83hk9-t500x500.jpg
- https://live.staticflickr.com/2431/3610687245_abe70a732a_b.jpg
- https://i.discogs.com/UAzTdaGD7vG8vYwTmDuit-ScDThMHkPIYa70iPl-roE/rs:fit/g:sm/q:90/h:600/w:599/czM6Ly9kaXNjb2dz/LWRhdGFiYXNlLWlt/YWdlcy9SLTY0NzI3/Mi0xNjk4OTU2Mzg4/LTczNTcuanBlZw.jpeg
- https://optimise2.assets-servd.host/vague-roadrunner/production/episode/6232-d10e-4ab2-9b01-6f0abf63a4e0.jpg?w=1200&h=1200&auto=compress%2Cformat&fit=crop&fp-x=0.5&fp-y=0.5&dm=1617365805&s=baf45d56a4585a1606abbd3af507d355
- https://d2cup43q5qzhdv.cloudfront.net/c458-1e03-45db-8335-daaa2eb37bfc.jpeg
multipleColumn: true
title: UK Dubstep / Liquid Garage / Purple Music
tags:
- playlists
- playlist
date: 2023-11-15
hideTitle: true
hideExif: true
hideDate: true
---

# UK Dubstep / Liquid Garage / Purple Music

- [Spotify](https://open.spotify.com/playlist/6ARGBjrafDzsMykCeZRoxM?si=8461a2d97b374eaf)
- [Tidal](https://tidal.com/browse/playlist/1fb97252-cce2-49f2-ad1d-7220d4c987ea)

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sed ornare arcu, nec semper felis. Mauris porta nunc massa. In et erat placerat, iaculis ipsum vitae, elementum diam. Cras lacus nisl, convallis vel mattis a, consectetur at turpis. Nam tristique quam lacus. Morbi id tristique magna, a feugiat sem. Phasellus congue vel metus eget aliquet. Praesent at placerat tortor. Vivamus pharetra euismod magna, ut tincidunt eros euismod vel.

Curabitur pulvinar elementum lectus. Integer lobortis porta diam. Cras dapibus a enim vitae convallis. Vestibulum viverra scelerisque iaculis. Donec suscipit, ligula ac luctus iaculis, urna sem faucibus augue, eget viverra mi massa ac felis. Duis a varius lectus. Praesent imperdiet mollis lectus, sed euismod augue pellentesque vitae. Vestibulum massa erat, venenatis id justo nec, fermentum pellentesque erat. Donec sed maximus mi.

Duis facilisis quam et ligula aliquet, quis elementum risus egestas. Cras euismod dolor nibh, non sodales enim porta vel. Praesent quis augue non nunc placerat rhoncus. Nunc mollis condimentum urna et porta. Sed mattis euismod vestibulum. Donec velit ipsum, facilisis ut augue non, suscipit pulvinar purus. Mauris ac ipsum a urna dapibus iaculis eget eu nisl. Integer luctus molestie diam ut sollicitudin. Fusce interdum eget libero dictum interdum. Praesent mauris purus, vehicula ut odio vitae, posuere pharetra ex. Aliquam fermentum faucibus dictum.
